<?php
/**
  Template Name: About
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package My Great Theme
 */
get_header();
?>
<div id="contenido_page">

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <section id="About">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 1">
                            <?php while (have_posts()) : the_post(); ?>

                                <h1><?php the_title(); ?></h1>
                            </div>


                            <div class="col-md-12 col-xs-12">
                                <div class="background-texto-about">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; // end of the loop. ?>

                    </div>
                </div> 



            </section>

        </main><!-- #main -->
    </div><!-- #primary -->
</div>

<?php get_footer(); ?>