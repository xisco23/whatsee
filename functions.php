<?php
/**
 * Whatsee Theme functions and definitions
 *
 * @package Whatsee Theme
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'whatsee_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function whatsee_theme_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on My Great Theme, use a find and replace
	 * to change 'whatsee-theme' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'whatsee-theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails', array('negocio') );
	add_image_size( 'negocio-marker', 30, 30, true );
	add_image_size( 'negocio-thumb', 300, 250, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'whatsee-theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'whatsee_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // whatsee_theme_setup
add_action( 'after_setup_theme', 'whatsee_theme_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function whatsee_theme_widgets_init() {
	$args1 = array(
		'id'            => 'front-page-sidebar',
		'name'          => __( 'Sidebar front-page', 'whatsee-theme' ),
		'description'   => __( 'Section Widgets en 3 columnas.', 'text_domain' ),
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'before_widget' => '<aside id="%1$s" class="widget border-widget-front %2$s col-md-4 col-sm-4 col-xs-12">',
		'after_widget'  => '</aside>',
	);
        register_sidebar( $args1 );
        
        $args2 = array(
		'id'            => 'footer-sidebar',
		'name'          => __( 'Sidebar Footer', 'whatsee-theme' ),
		'description'   => __( 'Section Widgets footer en 3 columnas.', 'whatsee-theme' ),
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'before_widget' => '<aside id="%1$s" class="widget %2$s col-md-4 col-xs-12">',
		'after_widget'  => '</aside>',
	);
	
	
        register_sidebar( $args2 );
		
	 $args3 = array(
		'id'            => 'blog-sidebar',
		'name'          => __( 'Sidebar Blog', 'whatsee-theme' ),
		'description'   => __( 'Section Widgets footer en 3 columnas.', 'whatsee-theme' ),
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'before_widget' => '<aside id="%1$s" class="widget %2$s ">',
		'after_widget'  => '</aside>',
	);
	
	
        register_sidebar( $args3 );
}
add_action( 'widgets_init', 'whatsee_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function whatsee_theme_scripts() {


	//CARGAMOS NUESTRO CSS
	wp_enqueue_style( 'bootstrap-style', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css', array(),'1212',false);
	wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/style.css', array(),'2',false);
	wp_enqueue_style( 'slider', get_template_directory_uri() . '/assets/css/slider.css', array(),'2',false);
    wp_enqueue_style('fonts-awesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.css', array(), '2', false);

    /*RAFA*/   
    wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/assets/css/custom-style.css', array(), '20120206', false );
	wp_enqueue_script( 'whatsee-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206',false );
	wp_enqueue_script( 'whatsee-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'whatsee_theme_scripts' );

/** Enqueue Geo scripts.
*/
function geo_postions_scripts() {
	
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ( 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js' ), false, null, true );
    wp_enqueue_script( 'jquery' );

	//inspiration http://www.noeltock.com/web-design/wordpress/events-custom-post-types-fullcalendar-json-wordpress/
	
//Add a comment to this line
	wp_enqueue_script( 'bootstrap-script', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js', array(), '3.3.4', true );
	wp_enqueue_script( 'jquery-migrate', 'code.jquery.com/jquery-migrate-1.2.1.min.js', array(), '1.2.1', true );
	wp_enqueue_script( 'map-script', 'http://maps.googleapis.com/maps/api/js', array(), '20120206', true );
	wp_enqueue_script( 'srly-js', get_template_directory_uri() . '/assets/js/jquery.sly.js', array(), '2', true );
	wp_enqueue_script( 'plugin-js', get_template_directory_uri() . '/assets/js/vendor/plugins.js', array(), '2', true );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.js', array(), '2', true);
	
	
	wp_enqueue_script( 'geo-script', get_template_directory_uri() . '/assets/js/geo.js', array('jquery','map-script'), '1', false );
	
	wp_localize_script( 'geo-script', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	

}
add_action( 'wp_enqueue_scripts', 'geo_postions_scripts' );

/**
* Register Nav menu bootstrap
*/

require_once('wp_bootstrap_navwalker.php');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * CPT-negocios
 */
require get_template_directory() . '/inc/post-type-function.php';

/**
* Personalizar formularios
*/
require get_template_directory() . '/inc/custom-form.php';

/**
* Loops GEO y Comment
*/

require get_template_directory() . '/inc/loops.php';

/**
* Aádimos metaboxes en el administrador
*/
require get_template_directory() . '/inc/metaboxes-admin.php';

/**
* Aádimos taxonomia negocio en el administrador
*/
require get_template_directory() . '/inc/negocio-taxonomy.php';