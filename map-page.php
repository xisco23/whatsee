<?php
/**
  Template Name: Map testing
 *
 * @package Whatsee Theme
 */
get_header();
?>

<div id="cuerpo">

    <nav id="menu_perfil_empresa">
        <div id="logo_empresa">
            <img src="" style="width:100%;" alt="" title="" id="imagen_empresa">
        </div>
        <div id="contenido_menu_empresa">
            <h1></h1>
            <p class="contenido_empresa"></p>
            <span class="direccion_empresa"></span> <span class="telefono_empresa"></span> 

            <p class="empresa">Cerrar</p>
        </div>
    </nav>



    <section id="map-canvas"></section>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div id="whatseecoment">

                <div id="flecha" class="anim_img_flecha">
                    <img src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/flecha_arriba.png">
                </div>

                <div id="panel_menu" style="display: none;">
                    <p class="form-allowed-tags" style="color:#428bca;">Filtra y busca tus intereses o deja un comentario localizado durante 24h en nuestro mapa, jamás mostraremos tu email.</p>

<!--                    <div class="col-md-6 col-xs-12">
                    </div>
                    <div class="col-md-6 col-xs-12">-->

                        <?php
                        $args = array(
                            'show_option_all' => 'Filtrar por categorias',
                            'taxonomy' => 'categoria',
                            'selected' => get_query_var('cat'),
                        );
                        ?>

                        <?php wp_dropdown_categories($args); ?>
                        <script type="text/javascript">
                            <!--
        var dropdown = document.getElementById("cat");
                            function onCatChange() {
                                if (dropdown.options[dropdown.selectedIndex].value >= 0) {

                                    location.href = "<?php
                        global $post;
                        $post_slug = $post->post_name;

                        echo esc_url(home_url('/' . $post_slug));
                        ?>?cat=" + dropdown.options[dropdown.selectedIndex].value;
                                }
                            }
                            dropdown.onchange = onCatChange;
-->
                        </script>


                        <?php
// If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || get_comments_number()) :
                            $comments_args = array(
                                // change the title of send button 
                                'label_submit' => 'Comenta',
                                'title_reply' => '',
//                                'comment_notes_before' => '<p class="form-allowed-tags" style="color:#428bca;">Filtra y busca tus intereses o deja un comentario localizado durante 24h en nuestro mapa, jamás mostraremos tu email.</p>',
                                'comment_notes_after' => '',
                            );


                            comment_form($comments_args);

                        endif;
                        ?>
                    <!--</div>-->
                    <!--<div id="filtrado_menu">
                    
                            <div id="scrolling">
                                
                                <ul>
                                    <li>
                                        Swipe
                                    </li>
                 
                                </ul>
                            </div>
                    </div>
                    
                    
                    <div class="kilometros">
                        <div class="range-slider">
                            <input class="input-range" type="range" value="25" min="1" max="50">
                            <span class="range-value"></span>
                        </div> 
                    </div>
                    <div class="buscador">
                        <p><input type="text" id="buscador" placeholder="Buscar..."></p>
                    </div>
                    <input type="hidden" id="rang_val" value="">
                    <input type="hidden" id="menu_val" value="">
                    -->
                    <!--<div class="form_coment col-md-6 col-xs-12">
                    
                        <               <form action="http://whatsee.es/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
                                            <input placeholder="Escribe aqui tu nombre..." style="float: left;left: 103px;" id="author" name="author" type="text" value="" size="30" aria-required="true">
                                            <input placeholder="Escribe aqui tu email..."id="email" name="email" type="email" value="" size="30" aria-describedby="email-notes" aria-required="true">
                                            <input class="latitud" name="lat" type="hidden" value="39.7179753">
                                            <input class="longitud" name="lng" type="hidden" value="2.921468">
                                            <textarea style="resize: none;height: 84px;background: transparent;" placeholder="Escribe aqui tu comentario..." id="comment" name="comment" cols="45" rows="8" aria-describedby="form-allowed-tags" aria-required="true"></textarea>											<p class="form-submit">
                        
                        
                                        </form>
                                    </div>
                                    
                    </div>-->
                </div><!-- Class whatseecoment-->
            </div><!-- Class Col-xs-12 col-md-12 -->
        </div><!-- Class row-->


    </div>

</div><!-- Class Cuerpo -->


<script>
                            //$(".menu").load('<?php // bloginfo('template_url');                    ?>/menu.php');
                            $("#flecha img").on("click", function () {
                                height_page = $(window).height();
                                if(height_page <=600)
                                {
                                    height_page_re = height_page-55;
                                }else{
                                    height_page_re = "450";
                                }
                                $("#panel_menu").slideToggle("fast", function () {
                                    if ($(this).is(":visible")) {

                                        $("#flecha img").attr('src', '<?php echo get_bloginfo('template_directory'); ?>/assets/img/flecha_abajo.png');
                                        
//                                        height_page_redimen = "300px";
                                        $(this).css({
                                            "background": "rgba(255,255,255,0.7)",
                                            "position": "absolute",
                                            "font-size": "10px",
                                            "height": height_page_re,
                                            "bottom": "0",
                                            "width": "100%"
                                        });

                                        $("#flecha img").css({
                                            "position": "relative",
                                            "z-index": "999999",
                                        });
                                        $("#flecha img").animate({
                                            bottom: height_page_re
                                        }, {
                                            opacity: '0.5'
                                        });
                                    } else {
                                        $("#flecha img").attr('src', '<?php echo get_bloginfo('template_directory'); ?>/assets/img/flecha_arriba.png');
                                        $("#flecha img").css({
                                            "position": "relative"
                                        });
                                        $("#flecha img").animate({
                                            bottom: '0',
                                            opacity: '1'
                                        });

                                    }
                                });
                            });
                            /*var range = $('.input-range'),
                             value = $('.range-value');
                             
                             value.html(range.attr('value'));
                             
                             range.on('input', function () {
                             value.html(this.value);
                             var range = $(".range-value").html();
                             var rango = $("#rang_val").val(range);
                             var menu_val = $("#menu_val").val();
                             var range_val = $("#rang_val").val();
                             var buscador = $("#buscador").val();
                             //ajax(menu_val, range_val, buscador);
                             });*/
</script>
<script>
//    /*Script Menu slider*/
//    setInterval(function () {
//        var contenido = $(".itemslide-active").html();
//        $("#ala").html(contenido);
//    });
</script>

<script>

</script>