<?php
define('WP_USE_THEMES', false);


function getDistancia($lat, $lng, $distance, $earthRadius = 6371)
{
    $return = array();
     
    // Los angulos para cada dirección
    $cardinalCoords = array('north' => '0',
                            'south' => '180',
                            'east' => '90',
                            'west' => '270');

    $rLat = deg2rad($lat);
    $rLng = deg2rad($lng);
    $rAngDist = $distance/$earthRadius;

    foreach ($cardinalCoords as $name => $angle)
    {
        $rAngle = deg2rad($angle);
        $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
        $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));

         $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                'lng' => (float) rad2deg($rLonB));
    }

    return array('min_lat'  => $return['south']['lat'],
                 'max_lat' => $return['north']['lat'],
                 'min_lng' => $return['west']['lng'],
                 'max_lng' => $return['east']['lng']);
}






// WP_Query arguments
$args = array (
	'post_type' => 'negocio',
);

// The Query
$map_markers = new WP_Query( $args ); 

?>



<?php if ($map_markers->have_posts()) : ?>
<?php //$jsondata = array(); ?>
<?php while ($map_markers->have_posts()) : $map_markers->the_post(); ?>
   
    <?php 
		
		$jsondata[] = array(
            
            'title' => get_the_title(),
			'content' => get_the_content(),
			'latitud' => get_post_meta($post->ID,'Latitud',true),
			'longitud' => get_post_meta($post->ID,'Longitud',true)
			
        );
	
	?>
    
    
<?php endwhile; 
wp_reset_query();

ob_clean();
echo json_encode($jsondata);
exit();

// ver http://stackoverflow.com/a/21619045
?>
<?php endif; ?>