module.exports = function(grunt) {

    // All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Javascript concatenation
     /*   concat: {
            dist: {
                src: [
                    'src/js/bootstrap.js',
                    'src/js/jquery.json.js',
                    'src/js/custom.js',
                ],
                dest: 'src/js/app.js',
            },
            html5: {
                src: [
                    'src/js/html5shiv.js',
                    'src/js/respond.min.js',
                ],
                dest: 'src/js/html5.js',
            }
        },

        // Javascript minification
        uglify: {
            dist: {
                files: {
                    'assets/js/app.min.js': [
                        'src/js/app.js',
                    ],
                }
            }
        },
*/
        // CSS minification
        cssmin: {
            combine: {
                options: {
                    // Remove comments
                    keepSpecialComments: 0
                },
                // Everything in src/css is combined to a single assets/css/style.css
                files: {
                  'assets/css/style-pre.css': ['src/css/*.css']
                }
            }
        },

        // LESS Compilation
        less: {
            build: {
                files: {
                    'assets/css/custom-style.css': 'src/less/custom-style.less',
					
                }
            },
        },

        // CSS autoprefixer
       autoprefixer: {
            options: {
              browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie 8', 'ie 9']
            },
            your_target: {
                src: 'assets/css/style-pre.css',
                dest: 'assets/css/style.css'
            },
        }
  /*      watch: {
            less: {
                files: ['src/less/*.less'],
                tasks: ['less'],
            },
            css: {
                files: ['src/css/*.css'],
                tasks: ['cssmin', 'autoprefixer'],
            },
            js: {
                files: ['src/js/custom.js'],
                tasks: ['uglify','concat'],
            },
        }*/
    });

    // Tell Grunt we plan to use these plugins.
    // grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-wp-i18n');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Tell Grunt what to do when we type "grunt" into the terminal.
    // grunt.registerTask('localdev', ['watch']);
    grunt.registerTask('cssonly', ['less', 'cssmin', 'autoprefixer']);
    grunt.registerTask('jsonly', ['concat', 'uglify']);
    grunt.registerTask('default', ['cssonly', 'jsonly']);

};
