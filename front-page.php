<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package My Great Theme
 */
get_header();
?>
<!-- contenido  con top-margin por el menu fijo -->
<div id="contenido_page">
    <!-- Imagen mapa inicio -->
    <section class="background-img-home">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php while (have_posts()) : the_post(); ?>

                        <img class="logo_wahtsee" src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/logo_whatsee.png">1>

                        <?php the_content(); ?>

                    <?php endwhile; // end of the loop. ?>
                </div>
            </div>
        </div>       
    </section>  

    <!-- seccion de caracteristicas -->

    <section id="caracteristicas">
        <div class="container">
            <div class="row contenido-pagina">

                <div class="col-md-4 col-xs-12 efecto-caja">

                    <div class="caja-icono">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary" style="color: green;"></i>
                            <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>

                    <div class="caja-texto">
                        <h3>Geolocalización</h3>

                        <p>

Sistema de localización desarrollado por el equipo de wahtsee, junto a los mapas de google maps creamos este maravilloso servicio                         </p>
                    </div>

                </div>
                <div class="col-md-4 col-xs-12 efecto-caja">

                    <div class="caja-icono">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary" style="color:purple;"></i>
                            <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>

                    <div class="caja-texto">
                        <h3> Eventos y Ofertas </h3>

                        <p>
                            
                          Publica tus ofertas y eventos, las personas de tu alrededor podrán visualizarlas en el mapa a tiempo real, desde la gente local hasta turistas podrán beneficiarse de ello . 
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 efecto-caja">

                    <div class="caja-icono">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary" style="color: gray;"></i>
                            <i class="fa fa-comments-o fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>

                    <div class="caja-texto">
                        <h3> Social </h3>

                        <p>
                     Deja tu comentario en el mapa y los demás podrán verlo, podrás conocer gente y aprovechar mejores ofertas en grupo.                        </p>
                    </div>
                </div>


                <div class="col-md-4 col-xs-12 efecto-caja">

                    <div class="caja-icono">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary" style="color: olive;"></i>
                            <i class="fa fa-arrows fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>

                    <div class="caja-texto">
                        <h3> Responsive</h3>

                        <p>
                            Este servicio se puede visualizar desde cualquier dispositivo, aprovecha tu movil o ipad y encuntra las ofertas que te rodean.
                        </p>
                    </div>

                </div>
                <div class="col-md-4 col-xs-12 efecto-caja">

                    <div class="caja-icono">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary" style="color: brown;"></i>
                            <i class="fa fa-archive fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>

                    <div class="caja-texto">
                        <h3>Perfil</h3>

                        <p>
                           Este servicio se puede utilizar desde cualquier dispositivo, aprovecha tu Mobil o hipad y encuentra las ofertas que te rodean.                        </p>
                    </div>

                </div>
                <div class="col-md-4 col-xs-12 efecto-caja">

                    <div class="caja-icono">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary "style="color: black;"></i>
                            <i class="fa fa-building fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>

                    <div class="caja-texto">
                        <h3>sencillo</h3>

                        <p>
                          Sistema sencillo de usar, gestiona tu empresa desde un panel diseñado para facilitar su uso al cliente, podrás crear ofertas y administrar tu perfil de una manera rápida.                        </p>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <section id="servicios">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12 col-xs-12 ">
                    <?php while (have_posts()) : the_post(); ?>

                        <h2 class="h2Servicios">Confia en nuestro servicio</h2>
                        <p class="pServicios">
                            Todos juntos podemos formar una gran comunidad, ayudanos a facilitarte la vida.
                        </p>
                    <?php endwhile; // end of the loop. ?>
                </div>
            </div>
        </div>

        <div class="container text-center">
            <div class="CajaServicio">
                <div class="row">
                    <?php dynamic_sidebar('front-page-sidebar'); ?>
                </div>
            </div>     
        </div>

    </section>


   

    <section id="subscribete">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h2>Subscribete</h2>
                    <p>Si quieres recibir información sobre el servicio introduce tu email.</p>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <form method="post">
                        <input type="email" placeholder="Email" style="width: 80%;">
                    <input type="submit" value="Enviar" style="width: 20%;">
                    </form>
                </div>
                <div
            </div>
        </div>

</section>
</div>

<?php get_footer(); ?>
