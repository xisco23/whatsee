<?php
/**
 * The template for displaying 404 pages (not found).
  Template name: 404
 * @package Whatsee Theme
 */
get_header();
?>
<div id="contenido_page">

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <section class="error-404 not-found">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="border">
                                <header class="page-header">
                                    <h1 class="page-title"><?php _e('404', 'whatsee-theme'); ?></h1>
                                    <h2><?php _e('The url does not exist, try again', 'whatsee-theme'); ?></h2>


                                </header><!-- .page-header -->

                                <div class="page-content">

                                    <p style="margin-bottom: 20px;">busque en nuestra secciones:</p>

                                    <?php get_search_form(); ?>

                                </div><!-- .page-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- .error-404 -->

        </main><!-- #main -->
    </div><!-- #primary -->
</div>
<?php get_footer(); ?>
