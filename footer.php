<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package My Great Theme
 */
?>

</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar('footer-sidebar'); ?>
            </div>
        </div>
    
    <div class="col-md-12 col-xs-12 text-left barraFooter" style="padding:0;">
        <p>Copyright - <a href="#"> Politica de privacidad</a></p>
        

    </div>
</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
