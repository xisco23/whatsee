<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package My Great Theme
 */
get_header();
?>


<!--
<section id="global">
       
            <div class="row-fluid fondo">
                <div class="tituloInicio">
                    <h1>WHATSEE</h1>
                </div>
                
            </div>
        </div>-->
<section id="global">
    <div class="container-fluid fondo">
        <div class="row top-buffer">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="CajaTitulo">
                    <h1>WHATSEE</h1>
                    <h2>Disfrute de la sencillez que Whatsee le otorga,
                        la ultima experiencia en geolocalizacion con diversas opciones que facilitarna su uso. 
                    </h2>

                    <h2>Sencillez unidas con la complejidad, crean este marivilloso tema para uso y disfruto de todos.

                    </h2>
                </div>

            </div>       
            <div class="col-md-6 col-sm-12 col-xs-12">
                <button>comenzar</button>

            </div>       

        </div>

    </div>
    <div class="barraFooter">

    </div>
</section>



<?php get_footer(); ?>
