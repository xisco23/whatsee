$(document).ready(inicializarEventos);


function inicializarEventos() {

    initialize();




}


$(document).on('click', '.navbar-collapse.in', function (e) {
    if ($(e.target).is('a')) {
        $(this).collapse('hide');
    }
});


var infowindow = new google.maps.InfoWindow();


function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}


function createMarker(lat, lon, html, logo) {
    var newmarker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lon),
        map: map,
        icon: logo
                //title: html
    });

    var boxText = document.createElement("div");
    boxText.style.cssText = "padding: 10px;";
    boxText.innerHTML = html;


    new google.maps.MarkerImage(logo,
            new google.maps.Size(100, 100),
            new google.maps.Point(0, 0),
            new google.maps.Point(100, 100)
            );


    google.maps.event.addListener(newmarker, 'click', function () {

        infowindow.setContent(boxText);
        infowindow.open(map, this);


    });

}



var map;
var markers = [];

function initialize() {

    //HTML5 geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {

            //Get geolocation vars latitud longitud
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;

            //var finalLatitude = latitude - 0.00040 ;
            var finalCenter = new google.maps.LatLng(latitude, longitude);
            var mapOptions = {
                zoom: 18,
                scrollwheel: false,
                center: finalCenter,
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            //Insetamos mapa y las opciones en el contenedor
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            // Create the search box and link it to the UI element.
            //var input = /** @type {HTMLInputElement} */(
            //document.getElementById('pac-input'));



            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input); 

            //Nos situamos en el mapa
            var pos = new google.maps.LatLng(latitude, longitude);

            //Marker de posicion geololaizada
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                title: "I am here!!"
            });


            $('.latitud').val(latitude);
            $('.longitud').val(longitude);





            //Recogemos las variables Ajax con la accion geovars, más información archivo functions.php 
            $.getJSON(MyAjax.ajaxurl + "?action=geovars", function (data) {

                //Cantidad de markers
                $.each(data, function (fieldName, fieldValue) {

                    var neighborhoods = [
                        new google.maps.LatLng(fieldValue.latitud, fieldValue.longitud),
                    ];

                    var cat_url = getUrlParameter('cat');




                    if ((fieldValue.id_taxonomia == cat_url)) {
                        //Por cada vecino un marker con la funcion createMarker
                        for (var i = 0; i < neighborhoods.length; i++) {
                            var infoMarker = "<p style='color: #428bca; font-weight:bold;text-transform:uppercase;'><img src='" + fieldValue.logo_empresa + "' alt='logo empresa' /> <span class='nombre_empresa'>" + fieldValue.title + "</span></p><p>" + fieldValue.content + "</p><p class='empresa' style='text-align:right;color:blue;'>m&aacute;s informaci&oacute;n<span class='id_empresa' style='display:none;'>" + fieldValue.id + "</span></p>";
                            createMarker(fieldValue.latitud, fieldValue.longitud, infoMarker, fieldValue.logo_empresa);
                        }

                    } else if (cat_url == '') {

                        for (var i = 0; i < neighborhoods.length; i++) {
                            var infoMarker = "<p style='color: #428bca; font-weight:bold;text-transform:uppercase;'><img src='" + fieldValue.logo_empresa + "' alt='logo empresa' /> <span class='nombre_empresa'>" + fieldValue.title + "</span></p><p>" + fieldValue.content + "</p><p class='empresa' style='text-align:right;color:blue;'>m&aacute;s informaci&oacute;n<span class='id_empresa' style='display:none;'>" + fieldValue.id + "</span></p>";
                            createMarker(fieldValue.latitud, fieldValue.longitud, infoMarker, fieldValue.logo_empresa);
                        }

                    }



                });
            });


            //Recogemos las variables Ajax con la accion geovars, más información archivo functions.php 
            $.getJSON(MyAjax.ajaxurl + "?action=geovars_comment", function (data) {

                //Cantidad de markers
                $.each(data, function (fieldName, fieldValue) {

                    var neighborhoods = [
                        new google.maps.LatLng(fieldValue.latitud, fieldValue.longitud),
                    ];



                    //Por cada vecino un marker con la funcion createMarker
                    for (var i = 0; i < neighborhoods.length; i++) {
                        var infoMarker = "<p class='empresa' style='color: #428bca; font-weight:bold;text-transform:uppercase;'>" + fieldValue.author + "</p><p>" + fieldValue.comment + "</p><p style='font-size:1em;'>Publicado: " + fieldValue.comment_date_gmt + "</p>";
                        createMarker(fieldValue.latitud, fieldValue.longitud, infoMarker, 'http://steeplemedia.com/images/markers/markerBlue.png');
                    }

                });
            });

            //Todo lo demas funciones API HTML5

            var boton_ficha = '.empresa';
            /*Script Menu Derecha*/
            var control = 0;
            ventana_ancho = "";
            ventana_alto = "";
            var tamano_ventana = setInterval(function () {
                ventana_ancho = $(window).width();
                ventana_alto = $(window).height();
            }, 1000);


            $(document).ready(function () {
                // Escuchar cambios de orientaci�n

            });
            $(document).on("click", boton_ficha, function (e) {
                var pagina = "#cuerpo";
                var menu = "#menu_perfil_empresa";
                var mapa_section = "#map-canvas";
                var whatseecoment = "#whatseecoment";


                //Recogemos las variables Ajax con la accion geovars, más información archivo functions.php 
                $.getJSON(MyAjax.ajaxurl + "?action=geovars", function (data) {

                    //Cantidad de markers
                    $.each(data, function (fieldName, fieldValue) {


                        var id_ajax = fieldValue.id;
                        var id_empresa = $(".id_empresa").text();

                        if (id_ajax == id_empresa) {

                            $('#contenido_menu_empresa h1').text(fieldValue.title);
                            $('#contenido_menu_empresa .contenido_empresa').text(fieldValue.contenido_empresa);
                            $('#contenido_menu_empresa .telefono_empresa').text(fieldValue.telefono);
                            $('#contenido_menu_empresa .direccion_empresa').text(fieldValue.direccion_empresa);
                            $('#imagen_empresa').attr('src', fieldValue.imagen_empresa);
                        }

                    });
                });


                if (control == 0) //El menu esta oculto
                {
                    /*Movil*/
                    if (ventana_ancho <= 600)
                    {
                        $(pagina).toggleClass("active");

                        $(whatseecoment).animate({
                            right: "500px"
                        });

                        $(mapa_section).animate({
                            right: "500px"
                        });
                        $(menu).css("width",ventana_ancho);
                        ventana_ancho_1 = ventana_ancho - 40;
                        $("#logo_empresa").css({
                           "width":ventana_ancho_1,
                           "height":"auto"
                        });
                        $(menu).animate({
                            right: "0"
                        });
                        
                    } else {
                        $(pagina).toggleClass("active");

                        $(whatseecoment).animate({
                            right: "500px"
                        });

                        $(mapa_section).animate({
                            right: "500px"
                        });
                        $("#logo_empresa").css({
                           "width":"480px",
                           "height":"300px"
                        });
                        $(menu).css("width","500px");
                        $(menu).animate({
                            right: "0"
                        });
                    }

                    control++;
                }
                else {
                    if (ventana_ancho <= 600)
                    {
                        $(whatseecoment).animate({
                            right: "0"
                        });
                        $(mapa_section).animate({
                            right: "0"
                        });
                        $(menu).animate({
                            right: "-500px"
                        });
                    } else {
                        $(whatseecoment).animate({
                            right: "0"
                        });
                        $(mapa_section).animate({
                            right: "0"
                        });
                        $(menu).animate({
                            right: "-500px"
                        });
                    }

                    control--;
                }

            });




        }, function () {
            handleNoGeolocation(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }

}


function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
    } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var options = {
        map: map,
        position: new google.maps.LatLng(60, 105)
    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);


}

