
<?php
/**
Template Name: Inicio
 *
 * @package My Great Theme
 */
get_header();
?>

<!-- inicio -->

<div id="inicio" class="fondo">
    <h1 class="titulo">Whatsee</h1>
    <h2 class="sub-titulo">Comenzar</h2>
    <div id="barraInferior" class="barraF">
        <div class="enlaceRegistro">
            <a href="#" class="iniciarRegistro">Registrate</a>
        </div>
        <div class="enlaceLogin">
            <a href="#" class="iniciarSession">Entrar</a>
        </div>
    </div>
</div><!-- #Inicio-->

<?php get_footer(); ?>