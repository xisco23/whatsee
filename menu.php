<div class="slider">
    <div id="content">
        <div id="sections" class="container">
            <div id="horizontal">
                <div class="slyWrap example2">

                    <!--                <div class="scrollbar">
                                        <div class="handle"></div>
                                    </div>-->

                    <div class="sly" data-options='{ "horizontal": 1, "itemNav": "forceCentered", "dragContent": 1, "scrollBy": 1, "easing": "easeOutBack" }'>
                        <ul class="big cfix" data-items="10"></ul>
                    </div>
                    <!--<ul class="pages"></ul>-->
                </div>
            </div><!--end:#vertical-->
        </div><!--end:#sections-->
    </div><!--end:#content-->
</div>
<div class="kilometros">
    <div class="range-slider">
        <input class="input-range" type="range" value="25" min="1" max="50">
        <span class="range-value"></span> Kilometros
    </div> 
</div>
<div class="buscador">
    <p><input type="text" id="buscador" placeholder="Buscar..."></p>
</div>
<input type="hidden" id="rang_val" value="">
<input type="hidden" id="menu_val" value="">



<!--Ajax-->
<script>
    $(function () {
        $('li').click(function () {
            var menu = $(this).html();
            $("#menu_val").val(menu);
            var menu_val = $("#menu_val").val();
            var range_val = $("#rang_val").val();
            var buscador = $("#buscador").val();
            //ajax(menu_val, range_val, buscador);
        });
        var range = $('.input-range'),
                value = $('.range-value');

        value.html(range.attr('value'));

        range.on('input', function () {
            value.html(this.value);
            var range = $(".range-value").html();
            var rango = $("#rang_val").val(range);
            var menu_val = $("#menu_val").val();
            var range_val = $("#rang_val").val();
            var buscador = $("#buscador").val();
            //ajax(menu_val, range_val, buscador);
        });
        $("#buscador").keyup(function(){
            var menu_val = $("#menu_val").val();
            var range_val = $("#rang_val").val();
            var buscador = $("#buscador").val();
            //ajax(menu_val, range_val, buscador);
        });
        /*ajax*/
        function ajax(menu, rang, buscador)
        {
            var datos = $.ajax({
                url: "filtros.php",
                type: 'POST',
                data: {
                    menu: menu,
                    range: rang,
                    buscador:buscador
                },
                success: function (data) {
                    $("#cuerpo").html(data);
                }
            });
        }
    });
</script>