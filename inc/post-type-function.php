<?php

/**
AÑADIDO POR FABIO
*/
if ( ! function_exists('creacion_de_negocios') ) {

// Register Custom Post Type
function creacion_de_negocios() {

	$labels = array(
		'name'                => _x( 'Negocios', 'Post Type General Name', 'whatsee' ),
		'singular_name'       => _x( 'Negocio', 'Post Type Singular Name', 'whatsee' ),
		'menu_name'           => __( 'Crear Negocio', 'whatsee' ),
		'parent_item_colon'   => __( 'Parent Item:', 'whatsee' ),
		'all_items'           => __( 'Negocios Creados', 'whatsee' ),
		'view_item'           => __( 'View Item', 'whatsee' ),
		'add_new_item'        => __( 'Add New Item', 'whatsee' ),
		'add_new'             => __( 'Añadir Negocio', 'whatsee' ),
		'edit_item'           => __( 'Edit Item', 'whatsee' ),
		'update_item'         => __( 'Update Item', 'whatsee' ),
		'search_items'        => __( 'Search Item', 'whatsee' ),
		'not_found'           => __( 'Not found', 'whatsee' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'whatsee' ),
	);
	$args = array(
		'label'               => __( 'negocio', 'whatsee' ),
		'description'         => __( 'Post Type Description', 'whatsee' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields','excerpt' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'negocio',
		/*'map_meta_cap'		  => true,*/
		'capabilities' 		  => array(
										'read' => 'read_negocio',
										'publish_posts' => 'publish_negocio',
										'edit_posts' => 'edit_negocio',
										'edit_others_posts' => 'edit_others_negocio',
										'read_private_posts' => 'read_private_negocio',
										'edit_post' => 'edit_negocio',
										'delete_post' => 'delete_negocio',
										'read_post' => 'read_negocio',
										
										
			),
	);
	register_post_type( 'negocio', $args );

}

// Hook into the 'init' action
	add_action( 'init', 'creacion_de_negocios');

}


//USER ROLE USER_NEGOCIO

$result = add_role( 'administrator', 'Administrator', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
	'delete_posts' => true, 
	'edit_pages' => true, // Allows user to edit pages
	'edit_others_posts' => true, // Allows user to edit others posts not just their own
));
$result = add_role( 'propietario_negocio', 'Propietario Negocio', array(
    'read' => true, // True allows that capability
    'edit_posts' => true,
	'delete_posts' => true, 
	'edit_pages' => true, // Allows user to edit pages
	'edit_others_posts' => false, // Allows user to edit others posts not just their own
	
));



function add_capability() {
	global $wp_roles;
    // gets the author role
    $role = get_role( 'administrator' );
	
	$role->add_cap('read');
    $role->add_cap( 'edit' ); 
	
	
	
	
    // This only works, because it accesses the class instance.
    
	 
}
add_action( 'admin_init', 'add_capability');


?>