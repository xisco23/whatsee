<?php


function geo_postions_loop() {
	
	global $post;


	$product_id = 7;

	
	// WP_Query arguments
	$args = array (
		'post_type' => 'negocio',
		'cache_results'  => true,
		'update_post_meta_cache' => true,
		'update_post_term_cache' => true,
	
	);
	
	// The Query
	$map_markers = new WP_Query( $args ); 
	
	?>
	
	<?php if ($map_markers->have_posts()) : ?>
	<?php $jsondata = array(); ?>
	<?php while ($map_markers->have_posts()) : $map_markers->the_post(); ?>
	   
	    <?php 
			
			$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "negocio-thumb",true );
		
			$logo = wp_get_attachment_image_src( get_post_meta( $post->ID, '_cmb_logo_empresa_id', true), 'negocio-marker' );

			
			$jsondata[] = array(
			
				'id'	=> get_the_ID(),
	            'title' => get_the_title(),
				'content' => get_the_content(),
				'logo_empresa' => $logo[0],
				'imagen_empresa' => $thumbnail[0],
				'latitud' => get_post_meta($post->ID,'_cmb_latitud_empresa',true),
				'longitud' => get_post_meta($post->ID,'_cmb_longitud_empresa',true),
				'telefono' => get_post_meta($post->ID,'_cmb_telefono_empresa',true),
				'contenido_empresa' => get_post_meta($post->ID,'_cmb_contenido_empresa',true),
				'direccion_empresa' => get_post_meta($post->ID,'_cmb_direccion_empresa',true),
				'id_taxonomia' => wp_get_post_terms( $post->ID, 'categoria',array("fields" => "ids"))
				
	        );
			
		?>
	    
	    
	<?php endwhile; 
	wp_reset_query();
	
	ob_clean();
	echo json_encode($jsondata);
	wp_die();
	// ver http://stackoverflow.com/a/21619045
	?>
	<?php endif;
}
add_action( 'wp_ajax_geovars', 'geo_postions_loop' );
add_action( 'wp_ajax_nopriv_geovars', 'geo_postions_loop' );





//CONSULTA COMENTARIOS DE LOS USUARIOS, RESULTADOS POR JSON
function page_comment_postions_loop() {
	global $post;
	$hoy = date("d");
	// WP_Query arguments
	$args = array (
		'post_type' => 'page',
		'cache_results'  => true,
		'update_post_meta_cache' => true,
		'update_post_term_cache' => true,
		'date_query' => array(                  												  
			  array(
				'day' => $hoy, 
				),
		),      
	);


	// The Query
$comments_query = new WP_Comment_Query;

$comments = $comments_query->query( $args );

// Comment Loop
if ( $comments ) {
	$array_comment = array(); 
	/**echo '<pre>';
	print_r($comments);
	echo '</pre>';**/
	foreach ( $comments as $comment ) {
	 
	 	$array_comment[] = array(
				
				'author' => $comment->comment_author,
				'comment' => $comment->comment_content,
				'comment_date_gmt' => $comment->comment_date_gmt,
				'latitud' => get_comment_meta( $comment->comment_ID, 'lat', true ),
				'longitud' => get_comment_meta( $comment->comment_ID, 'lng', true )
		
		);
	 
	}
	//echo '<pre>';
	//print_r($array_comment);
	//echo '</pre>';
	//IMPRIMIMOS EL JSON
	wp_reset_query();
	
	ob_clean();
	echo json_encode($array_comment);
	wp_die();
	
} else {
	echo 'No comments found.';
}
	
	
}
add_action( 'wp_ajax_geovars_comment', 'page_comment_postions_loop' );
add_action( 'wp_ajax_nopriv_geovars_comment', 'page_comment_postions_loop' );

?>