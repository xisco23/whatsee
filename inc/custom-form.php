<?php

//QUITAR COSAS DE COMMENTS
function crunchify_init() {
	add_filter('comment_form_defaults','crunchify_comments_form_defaults');
}
add_action('after_setup_theme','crunchify_init');
 
function crunchify_comments_form_defaults($default) {
	unset($default['comment_notes_after']);
	return $default;
}



//AÑADIR CUSTOM FIELDS
function add_comment_fields($fields) {

    $fields['latitud'] = '<input class="latitud" name="lat" type="hidden" value="" />';
	$fields['longitud'] = '<input class="longitud" name="lng" type="hidden"  value=""/>';
    return $fields;
 
}
add_filter('comment_form_default_fields','add_comment_fields');


//AÑADIR EN LA BASE DE DATOS LAS COLUMNAS LATITUD Y LONGITUD
function add_comment_meta_values($comment_id) {
 
    if(isset($_POST['lat'])) {
        $lat = wp_filter_nohtml_kses($_POST['lat']);
        add_comment_meta($comment_id, 'lat', $lat, false);
    }
	
	    if(isset($_POST['lng'])) {
        $lng = wp_filter_nohtml_kses($_POST['lng']);
        add_comment_meta($comment_id, 'lng', $lng, false);
    }
 
}
add_action ('comment_post', 'add_comment_meta_values', 1);

/**
* Quitar input web
*/
function crunchify_disable_comment_url($fields) {
    unset($fields['url']);
	unset($fields['comment-notes']);
    return $fields;
}
add_filter('comment_form_default_fields','crunchify_disable_comment_url');


?>