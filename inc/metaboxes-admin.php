<?php


add_action( 'init', 'be_initialize_cmb_meta_boxes', 9999 );
function be_initialize_cmb_meta_boxes() {
    if ( !class_exists( 'cmb_Meta_Box' ) ) {
        require_once( 'custom_metaboxes/init.php' );
    }
}

function be_sample_metaboxes( $meta_boxes ) {
    $prefix = '_cmb_'; // Prefix for all fields
    $meta_boxes['whatsee_negocio_metabox'] = array(
        'id' => 'whatsee_negocio_metabox',
        'title' => 'Whatsee negocio metaboxes',
        'pages' => array('negocio'), // post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
		
		
        'fields' => array(
	
			 array(
                'name' => 'Logo empresa',
                'desc' => 'Logo para el mapa',
                'id' => $prefix . 'logo_empresa',
                'type' => 'file',
				'allow' => array( 'url', 'attachment' ) 
            ),
		
            array(
                'name' => 'Descripcion empresa',
                'desc' => 'Pequenyo texto descriptivo de la empresa',
                'id' => $prefix . 'contenido_empresa',
                'type' => 'text'
            ),
			
			array(
                'name' => 'Direccion',
                'desc' => 'Direccion de la empresa',
                'id' => $prefix . 'direccion_empresa',
                'type' => 'text'
            ),
			
			array(
                'name' => 'Telefono',
                'desc' => 'Telefono de la empresa',
                'id' => $prefix . 'telefono_empresa',
                'type' => 'text'
            ),
			
			array(
                'name' => 'Latitud',
                'desc' => 'Latitud de la empresa',
                'id' => $prefix . 'latitud_empresa',
                'type' => 'text'
            ),
			
			array(
                'name' => 'Longitud',
                'desc' => 'Longitud de la empresa',
                'id' => $prefix . 'longitud_empresa',
                'type' => 'text'
            ),
			
			
        ),
		
		
		
    );


	 $meta_boxes['whatsee_front_page_metabox'] = array(
        'id' => 'whatsee_front_page_metabox',
        'title' => 'Whatsee front_page metaboxes',
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
		
		
        'fields' => array(
	
            array(
                'name' => 'Titulo',
                'desc' => 'Titulo',
                'id' => $prefix . 'titulo',
                'type' => 'text'
            ),
			
			array(
                'name' => 'Subtitulo',
                'desc' => 'Direccion de la empresa',
                'id' => $prefix . 'subtitulo',
                'type' => 'text'
            ),
						
        ),
		
    );

    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'be_sample_metaboxes' );


?>